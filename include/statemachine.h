#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <stdbool.h>
#include <pthread.h>

/*
 * Configure some internal limit of the library
 */
#define STATEMACHINE_MAX_STATE 16
#define STATEMACHINE_MAX_STEP 8

//Statemachine function handler
typedef int (*state_machine_handler)(int nb_loop, void * data);

/*
 * T_statemachine_step is used by the internal statemachine.
 * Do not use it, use T_statemachine;
 *
 */
typedef struct {
	bool is_used;
	state_machine_handler handler;
	int next_state;
}T_statemachine_step;

/*
 * T_statemchine_state is used by the internal statemachine.
 * Do not use it, use T_statemachine;
 *
 */
typedef struct {
	bool is_used;
	T_statemachine_step step[STATEMACHINE_MAX_STEP];
}T_statemachine_state ;

/*
 * Declare all macro to setup the statemachine structure.
 * see the examples on how to use it.
 *
 */
#define STATEMACHINE_INITIALIZER \
	.is_initialized = true,      \
	.user_data = NULL, 

#define STATE(state_id) \
	.state[state_id]=

#define STEP(step_number,step_handler,next_state_id) \
	.step[step_number] = {                           \
	.is_used = true,                                 \
	.handler = step_handler,                         \
	.next_state = next_state_id                      \
	}, 

/*
 * Statemachine structure that will be allocated by the user.
 * User MUST keep it when the statemachine is running.
 *
 */
typedef struct {
	// Flag the keep track if the statemachine is init or not.
	bool is_initialized;

	// Internal statemachine STATE/STEP structure
	T_statemachine_state state[STATEMACHINE_MAX_STATE];

	// Thread data
	pthread_t sm_thread;

	// User data pointer
	void * user_data;
} T_statemachine;

int statemachine_init(T_statemachine * sm, void * user_data);
int statemachine_start(T_statemachine * sm);

#endif
