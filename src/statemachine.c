#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#include "statemachine.h"

/*
 * Function in which the state machine is running
 *
 */
static void *_statemachine_thread(void *data)
{
	bool running = true;
	uint32_t state = 0;
	uint32_t next_step = 0;
	uint32_t loop = 0;
	int ret;

	if(NULL == data){
		fprintf(stderr, "Error: data is null ptr in statemachine thread\n");
		return NULL;
	}

	T_statemachine * statemachine = (T_statemachine*)data;

	while(true == running){
		for(int step = 0; step < STATEMACHINE_MAX_STEP; step++){
			if(statemachine->state[state].step[step].is_used == true){
				printf("Execute state %d, step %d\n", state, step);

				/*
				 * Test if the handler is not NULL, if null break the statemachine
				 */
				if( NULL == statemachine->state[state].step[step].handler){
					printf("The handler in state: %d step: %d is NULL, exit machine", state, step);
					running = false;
					break;
				}

				/*
				 * Run the handler and manage the result
				 */
				ret = statemachine->state[state].step[step].handler(loop, statemachine->user_data);
				if(0 == ret){
					// The step handler returned 0, execute the next step of this state
					if((false == statemachine->state[state].step[next_step].is_used) || ((step + 1) == STATEMACHINE_MAX_STEP )){
						/*
						 * If next step is not used break the loop or is the max step number (8 by default).
						 * At the next while iteration the loop will start to step 0
						 */
						break;
					}
				}else if(1 == ret){
					// The step handler returned 1 here so we can go to next state
					state = statemachine->state[state].step[step].next_state;

					// Break the loop so we go in Next State but step 0
					break;
				}else{
					printf("Error running handler from State: %d, Step %d fail, return: %d\n", state, step, ret);
					running = false;
					break;
				}
			}
		}
	}

	// Technically the statemachine should never stop
	return NULL;
}

/*
 * Init statemachine and add the user_data
 * user_data CAN be null
 *
 */
int statemachine_init(T_statemachine * sm, void * user_data){
	// Test input argument
	if(NULL == sm){
		fprintf(stderr, "Error: statemachine struct is null ptr\n");
		return -1;
	}

	// Register user data
	sm->user_data = user_data;

	return 0;
}

/*
 * Start the statemachine thread
 *
 */
int statemachine_start(T_statemachine * sm){
	// Test input argument
	if(NULL == sm){
		fprintf(stderr, "Error: statemachine struct is null ptr\n");
		return -1;
	}

	// Create the statemachine thread and let it run
	if(pthread_create(&sm->sm_thread, NULL, _statemachine_thread, sm)) {
		fprintf(stderr, "Error creating thread\n");
		return -2;
	}

	return 0;
}
